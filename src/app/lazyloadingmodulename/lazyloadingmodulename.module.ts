import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazyloadingmodulenameRoutingModule } from './lazyloadingmodulename-routing.module';
import { LazyloadingmodulenameComponent } from './lazyloadingmodulename.component';


@NgModule({
  declarations: [LazyloadingmodulenameComponent],
  imports: [
    CommonModule,
    LazyloadingmodulenameRoutingModule
  ]
})
export class LazyloadingmodulenameModule { }
