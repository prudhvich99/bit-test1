import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LazyloadingmodulenameComponent } from './lazyloadingmodulename.component';

describe('LazyloadingmodulenameComponent', () => {
  let component: LazyloadingmodulenameComponent;
  let fixture: ComponentFixture<LazyloadingmodulenameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LazyloadingmodulenameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LazyloadingmodulenameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
