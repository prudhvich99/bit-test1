import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LazyloadingmodulenameComponent } from './lazyloadingmodulename.component';

const routes: Routes = [{ path: '', component: LazyloadingmodulenameComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyloadingmodulenameRoutingModule { }
