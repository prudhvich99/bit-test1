import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-component1',
  templateUrl: './component1.component.html',
  styleUrls: ['./component1.component.scss']
})
export class Component1Component implements OnInit {

  constructor(private shared1:SharedService) { }

  
  fundynamic(value:any)
  {
    this.shared1.emit<any>(value);
  }
  
  ngOnInit(): void {
    
  }

}
