import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-templatedriven',
  templateUrl: './templatedriven.component.html',
  styleUrls: ['./templatedriven.component.scss']
})
export class TemplatedrivenComponent implements OnInit {

  constructor() { }
  Name: string = "Hello";
  onSubmit(ref:any) {
    // window.alert(ref.controls.Email.value);
    console.log(ref.value);
    console.log(ref.controls.Name.value);
  }
  ngOnInit(): void {
  }

}
