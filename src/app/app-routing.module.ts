import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Component1Component } from './component1/component1.component';
import { Component2Component } from './component2/component2.component';
import { GettingpicsComponent } from './gettingpics/gettingpics.component';
import { IfelseComponent } from './ifelse/ifelse.component';
import { ChildComponent } from './parent/child/child.component';
import { ParentComponent } from './parent/parent.component';
import { RformsComponent } from './rforms/rforms.component';
import { TemplatedrivenComponent } from './templatedriven/templatedriven.component';

const routes: Routes = [
  {
    path:"rforms",
    component:RformsComponent
  },
  {
    path:"comp1",
    component:Component1Component
  },
  {
    path:"comp2",
    component:Component2Component
  },
  {
    path:"ifelse",
    component:IfelseComponent
  },
  {
    path:"gettingpics",
    component:GettingpicsComponent
  },
  {
    path:"templatedriven",
    component:TemplatedrivenComponent
  },
  {
    path:"parent",
    component:ParentComponent
  },
  {
    path:"child",
    component:ChildComponent
  },
  { path: 'lazyloading', loadChildren: () => import('./lazyloadingmodulename/lazyloadingmodulename.module').then(m => m.LazyloadingmodulenameModule) },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
