import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GettingpicsComponent } from './gettingpics.component';

describe('GettingpicsComponent', () => {
  let component: GettingpicsComponent;
  let fixture: ComponentFixture<GettingpicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GettingpicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GettingpicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
