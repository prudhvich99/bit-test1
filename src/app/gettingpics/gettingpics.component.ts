import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-gettingpics',
  templateUrl: './gettingpics.component.html',
  styleUrls: ['./gettingpics.component.scss']
})
export class GettingpicsComponent implements OnInit {
  
  constructor(private shared:SharedService) { }
  public data:any[]=this.shared.movies;
  ngOnInit(): void {
  
  }

}
