import { Injectable } from '@angular/core';
import {BehaviorSubject,Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public movies:any[]=[
    {name:'Endgame',language:'english',genre:'action',rating:'80%',image:'assets/1.jpg'},
    {name:'Pushpa',language:'telugu',genre:'action',rating:'87%',image:'assets/2.jpg'},
    {name:'Pink',language:'hindi',genre:'action',rating:'80%',image:'assets/3.jpg'},
    {name:'Spider-Man',language:'english',genre:'action',rating:'81%',image:'assets/4.jpg'},  
  ];
  constructor() { }
  public subject=new BehaviorSubject<any>('');
  emit<T>(data: T)
  {
    console.log(data);
    this.subject.next(data);
  }
  on<T>(): Observable<T>{
    return this.subject.asObservable();
  }
}
