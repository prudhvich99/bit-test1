import { NgModule } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RformsComponent } from './rforms/rforms.component';
import { Component1Component } from './component1/component1.component';
import { Component2Component } from './component2/component2.component';
import { IfelseComponent } from './ifelse/ifelse.component';
import { GettingpicsComponent } from './gettingpics/gettingpics.component';
import { TemplatedrivenComponent } from './templatedriven/templatedriven.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './parent/child/child.component';

@NgModule({
  declarations: [
    AppComponent,
    RformsComponent,
    Component1Component,
    Component2Component,
    IfelseComponent,
    GettingpicsComponent,
    TemplatedrivenComponent,
    ParentComponent,
    ChildComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
